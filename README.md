Using gpg, I generated a 2048-bit key (lasting 1 year),
following along with instructions found here:
     https://www.gnupg.org/gph/en/manual/c14.html
In this guide, 2048 bits is mentioned as the maximum 
recommended size, and after the in-class discussion, it
seemed to be a good idea to use this rather than the
initially stated 1048 bits.